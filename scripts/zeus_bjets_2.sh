#!/bin/bash
#PBS -l pmem=4500mb

#SBATCH -o ./Slurm/bjets_2/output_%a.out # STDOUT

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh

cd $SLURM_SUBMIT_DIR
cd ..
. source/FakeEfficiencyTool/scripts/setup.sh
cd $SLURM_SUBMIT_DIR



number=$SLURM_ARRAY_TASK_ID
case $number in
        0)
        run_number=313063
        ;;
        1)
        run_number=313067
        ;;
        2)
        run_number=313100
        ;;
        3)
        run_number=313107
        ;;
        4)
        run_number=313136
        ;;
        5)
        run_number=313187
        ;;
        6)
        run_number=313259
        ;;
        7)
        run_number=313285
        ;;
        8)
        run_number=313295
        ;;
        9)
        run_number=313333
        ;;
        10)
        run_number=313435
        ;;
        11)
        run_number=313572
        ;;
        12)
        run_number=313574
        ;;
        13)
        run_number=313575
        ;;
        14)
        run_number=313603
        ;;
        15)
        run_number=313629
        ;;
        16)
        run_number=313630
        ;;
        17)
        run_number=313688
        ;;
        18)
        run_number=313695
        ;;
        19)
        run_number=313833
        ;;
        20)
        run_number=313878
        ;;
        21)
        run_number=313929
        ;;
        22)
        run_number=313935
        ;;
        23)
        run_number=313984
        ;;
        24)
        run_number=314014
        ;;
        25)
        run_number=314077
        ;;
        26)
        run_number=314105
        ;;
        27)
        run_number=314112
        ;;
        28)
        run_number=314157
        ;;
        29)
        run_number=314170
        ;;
        30)
        run_number=MCZmumu1
        ;;
        31)
        run_number=MCZmumu2
        ;;
        32)
        run_number=MCZmumu3
        ;;
        33)
        run_number=MCZmumu4
        ;;
        34)
        run_number=MCZee1
        ;;
        35)
        run_number=MCZee2
        ;;
        36)
        run_number=MCZee3
        ;;
        37)
        run_number=MCZee4
        ;;
        38)
        run_number=MC2
        ;;
        39)
        run_number=MCZee
        ;;
        40)
        run_number=MCZmumu
        ;;
        41)
        run_number=MCttbar
        ;;
        42)
        run_number=MCttbar
        ;;

esac

#echo "${run_number}"
echo "top-xaod configs/configs_bjets_2/Config_bjets_2_$number.txt lists/list_data_00$run_number.txt  &>  logs/logs_bjets_2/log_bjets_2_00$run_number.txt &"
top-xaod configs/configs_bjets_2/Config_bjets_2_$number.txt lists/list_data_00$run_number.txt


