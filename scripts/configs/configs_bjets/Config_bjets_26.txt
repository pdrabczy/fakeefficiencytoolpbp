LibraryNames libTopEventSelectionTools libTopEventReconstructionTools libFakeEfficiencyTool


GRLDir GoodRunsLists

GRLFile data16_hip/20190708/data16_hip8TeV.periodAllYear_DetStatus-v105-pro22-13_Unknown_PHYS_HeavyIonP_All_Good.xml


#changed

PRWConfigFiles /eos/user/p/ppalni/TopShared/PRW_GRL/input/PileupReweightingMc15c-igb.root
PRWLumiCalcFiles GoodRunsLists/data16_hip/20190708/ilumicalc_histograms_None_313063-314170_OflLumi-HI-007.root

#MC16d configuration
#PRWConfigFiles_FS dev/AnalysisTop/PileupReweighting/user.iconnell.Top.PRW.MC16d.FS.v2/prw.merged.root
#PRWConfigFiles_AF dev/AnalysisTop/PileupReweighting/user.iconnell.Top.PRW.MC16d.AF.v2/prw.merged.root
#PRWActualMu_FS GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root
#PRWActualMu_AF GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root
#PRWLumiCalcFiles GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root

#NEvents 100

# here we set a number of flags for specific functions that can be set to True or False
DynamicKeys Debug,EventWeights,RateType,useChIDSF,doTruthEff,makeTree,Mu_probeTrigger,El_probeTrigger,Muon_d0,MuonLoose_d0,Muon_z0,MuonLoose_z0,Electron_d0,ElectronLoose_d0,Electron_z0,ElectronLoose_z0
Debug False
doTruthEff False
EventWeights True
makeTree True

# Tag and probe method to used
#RateType Real
#RateType FakeSFSS
#RateType FakeOFSS
RateType Fake1L
#RateType Fake3L

# Electron charge-ID config
useChIDSF False
UseElectronChargeIDSelection False

#Trigger-selection for probe leptons (None by default)
Mu_probeTrigger None
El_probeTrigger None
#El_probeTrigger mu15

UseAodMetaData True
IsAFII False

ElectronCollectionName Electrons
MuonCollectionName Muons
JetCollectionName AntiKt4EMTopoJets_BTagging201810
LargeJetCollectionName None
LargeJetSubstructure None
TauCollectionName None
PhotonCollectionName None
METCollectionName MET_Reference_AntiKt4EMTopo
TruthCollectionName TruthParticles
TruthJetCollectionName None
TruthElectronCollectionName None
TruthMuonCollectionName None
TruthMETCollectionName None
TrackJetCollectionName None

JetPt 25000.
JetEta 2.5
JVTWP Tight
UseBadBatmanCleaning True

PDFInfo False

ObjectSelectionName top::ObjectLoaderStandardCuts
OutputFormat top::EventSaverTop
OutputEvents SelectedEvents
OutputFilename output_26.root
PerfStats No

JetUncertainties_NPModel GlobalReduction
#JetUncertainties_BunchSpacing 25ns

ElectronID MediumLH
ElectronIDLoose LooseAndBLayerLH
ElectronIsolation FCTight
ElectronIsolationLoose None
ElectronVetoLArCrack False
ElectronPt 7000.
Electron_d0 5.
Electron_z0 0.5
ElectronLoose_d0 5.
ElectronLoose_z0 0.5 

MuonQuality Medium
MuonQualityLoose Medium
MuonIsolation FCTightTrackOnly
MuonIsolationLoose None
MuonPt 7000.
MuonEta 2.5
Muon_d0 3.
Muon_z0 0.5
MuonLoose_d0 7.
MuonLoose_z0 0.5

DoLoose Both
DoTight False

applyTTVACut False
ApplyTightSFsInLooseTree True

OverlapRemovalLeptonDef Loose
OverlapRemovalProcedure recommended

BTaggingWP MV2c10:FixedCutBEff_77
Systematics Nominal

UseGlobalLeptonTriggerSF True
GlobalTriggers 2016@mu15,mu15_L1MU6,mu15_L1MU10,e15_lhloose,e15_lhloose_nod0 2015@e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose,mu20_iloose_L1MU15_OR_mu50,mu26_ivarmedium_OR_mu50 2017@e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0,mu26_ivarmedium_OR_mu50 2018@e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0,mu26_ivarmedium_OR_mu50
GlobalTriggersLoose  2015@e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose,mu20_iloose_L1MU15_OR_mu50 2016@e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0,mu26_ivarmedium_OR_mu50 2017@e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0,mu26_ivarmedium_OR_mu50 2018@e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0,mu26_ivarmedium_OR_mu50

SELECTION Basic
INITIAL
GRL
GOODCALO
PRIVTX
NOBADMUON
JETCLEAN LooseBad
#GTRIGDEC
TRIGDEC HLT_e15_lhloose_nod0 HLT_e15_lhloose HLT_mu15 HLT_mu15_L1MU6 HLT_mu15_L1MU10

SELECTION Basic_Real
. Basic
Z
JET_N_BTAG MV2c10:FixedCutBEff_77 == 0


SELECTION Basic_FakeSS
. Basic
2LSS
JET_N_BTAG MV2c10:FixedCutBEff_77 >= 1


SELECTION Basic_Fake1L
. Basic
1L
MET < 20000


SELECTION Basic_Fake3L
. Basic
3L
Z
JET_N_BTAG MV2c10:FixedCutBEff_77 >= 1


SELECTION Selection_1
. Basic_Fake1L
JET_N_BTAG MV2c10:FixedCutBEff_77 >= 0
SAVE

SELECTION Selection_2
. Basic_Fake1L
JET_N_BTAG MV2c10:FixedCutBEff_77 >= 1
SAVE

SELECTION Selection_3
. Basic_Fake1L
JET_N_BTAG MV2c10:FixedCutBEff_77 >= 2
SAVE

SELECTION Selection_4
. Basic_Fake1L
JET_N_BTAG MV2c10:FixedCutBEff_77 >= 3
SAVE

